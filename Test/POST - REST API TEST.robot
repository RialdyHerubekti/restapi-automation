*** Settings ***
Library          RequestsLibrary
Library          JSONLibrary
Library          Collections
Library          DateTime
Resource         ../Resource/variable.robot
Documentation    REST API - POST 





*** Test Cases ***
TC01_Post_Request
    Create Session         PostTest               ${baseURL2}
    ${body}=               Create Dictionary      name=John Constantine      job=Engineer
    #${header}=           Create Dictionary      Content-Type=application/json
    ${post_response}=      POST on Session        PostTest                   /api/users                 json=${body}
    ${json_response}=      set variable           ${post_response.json()}
    ${name}=               Get Value from JSON    ${json_response}           $..name
    ${userId}=             Get Value from JSON    ${json_response}           $..id
    ${job}=                Get Value from JSON    ${json_response}           $..job
    ${created_at}=         Get Value from JSON    ${json_response}           $..createdAt
    ${currentDate}=        Get Current Date       UTC                        result_format=%Y-%m-%d     
    ${created_at}=         Convert Date           ${currentDate}             result_format=%Y-%m-%d 
    ${name}=               Get From List          ${name}                    0                          
    ${job}=                Get From List          ${job}                     0                          
    Should be Equal        ${currentDate}         ${created_at}
    Should be Equal        ${name}                John Constantine
    SHould be Equal        ${job}                 Engineer
    Should not be Empty    ${userId}




