*** Settings ***
Library          RequestsLibrary
Library          JSONLibrary
Library          Collections
Resource         ../Resource/variable.robot
Documentation    REST API - GET 





*** Test Cases ***
TC01_Get_Request
    Create Session       newTest                ${baseURL}
    ${get_response}=     GET on Session         newTest                   api/studentsDetails
    ${json_response}=    set variable           ${get_response.json()}
    log                  ${json_response}
    ${studentId}=        Get Value from JSON    ${json_response}          $.[0].id
    ${firstName}=        Get Value from JSON    ${json_response}          $.[0].first_name
    ${middleName}=       Get Value from JSON    ${json_response}          $.[0].middle_name
    ${lastname}=         Get Value from JSON    ${json_response}          $.[0].last_name
    ${dateOfBirth}=      Get Value from JSON    ${json_response}          $.[0].date_of_birth
    ${studentId}=        Get from List          ${studentId}              0
    ${firstName}=        Get from List          ${firstName}              0
    ${middleName}=       Get from List          ${middleName}             0
    ${lastname}=         Get from List          ${lastname}               0
    ${dateOfBirth}=      Get from List          ${dateOfBirth}            0
    Should be Equal      ${studentId}           ${1117492}
    Should be Equal      ${firstName}           test3
    Should be Equal      ${middleName}          middle3
    Should be Equal      ${lastname}            last4
    Should be Equal      ${dateOfBirth}         2022-05-13 00:00:00

